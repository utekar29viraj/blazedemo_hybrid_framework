As This is Hybrid Framework, which can run through both TestNG and Cucumber TestRunner

when you want to run test cases from TestNG follow the points:

 1. Go To src/test/java/TestRunner/TestRunner.java file
 2. if in import package "import io.cucumber.testng.CucumberOptions;" is commented uncomment it.
 3. Save the file to save changes
 4. run the last file testng.xml by rightclick -> Run as -> TestNg suit
 5. enjoy the testing
 
 
 
when you want to run test cases from Cucumber TestRunner follow the points:
 1. Go To src/test/java/TestRunner/TestRunner.java file
 2. if in import package "import io.cucumber.junit.CucumberOptions;" is commented uncomment it.
 3. Save the file to save changes 
 4. to run file, right click on "TestRunner.java" -> Run as -> Junit, (Test Runner.java) is available under scr/test/java/TestRunner/TestRunner.java
 5.enjoy the testing
 