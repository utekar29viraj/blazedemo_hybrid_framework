package Resources;


import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public class ExtentReporterNG {
  
	public static ExtentReports getReportObject() {
	 String report = System.getProperty("user.dir")+"/report/index.html";
	 ExtentSparkReporter reporter = new ExtentSparkReporter(report);
	 reporter.config().setDocumentTitle("Web-Automation Result");
	 reporter.config().setReportName("Test Result");
	 ExtentReports extent = new ExtentReports();
	 extent.attachReporter(reporter);
	 extent.setSystemInfo("Software Test Engineer", "Viraj D. Utekar");
	 return extent;
	}
	
	
}
