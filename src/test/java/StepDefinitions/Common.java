package StepDefinitions;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import PageObject.AboutPageObject;
import PageObject.ContactPageObject;
import PageObject.SignUpPageObject;
import Resources.Base;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Common extends  Base {
 
	public WebDriver driver;
	
	private static Logger log = LogManager.getLogger(Base.class.getName());
	
   @Before()
   public void on_test_start() {
	   System.out.println("test is started");
	   
   }
	
   @Given("user navigates to url")
   public void user_navigates_to_url()throws IOException {
	   driver = initializeDriver();
	   log.info("driver is been initialized");
	   driver.get(prop.getProperty("url"));
	   log.info("navigated to url site");
	   driver.manage().deleteAllCookies();
	   log.info("all cookies are been deleted");
	   driver.manage().window().maximize(); 
	   log.info("window size is been maximized");
   }
   
 
   @When("user click on about menu link")
   public void user_click_on_about_menu_link() {
	   AboutPageObject apo = new AboutPageObject(driver);
	   apo.getAboutMenuLink().click();
	   log.info("about menu link is been clicked");
   }

   @Then("the window popup is displayed")
	public void the_window_popup_is_displayed() {
		AboutPageObject apo = new AboutPageObject(driver);
       boolean windowPopupIsDisplayed = apo.getAboutWindowPop().isDisplayed();
	   if(windowPopupIsDisplayed) {
		   Assert.assertTrue(true);
		   log.info("window popup is displayed");
	   }
	}
   
  @And("user plays video")
	public void user_clicks_on_video() {
		AboutPageObject apo = new AboutPageObject(driver);
		apo.getVideoPlayOption().click();
		log.info("video is been played");
	}
	
  
  @Then("user click on close button")
	public void user_click_on_close_button() throws InterruptedException {
		AboutPageObject apo = new AboutPageObject(driver);
		apo.getCloseButton().click();
		Thread.sleep(2000);
		boolean windowPopupIsDisplayed = apo.getAboutWindowPop().isDisplayed();
		if(!windowPopupIsDisplayed) {
			Assert.assertTrue(true);
		}		
	}

  @When("^user click on contact menu option$") 
	public void user_click_on_contact_menu_option() { 
	  ContactPageObject cp = new ContactPageObject(driver);
	  cp.getContactMenuLink().click();
	  log.info("contact menu link is been clicked"); }

	   
  @Then("^contact form is displayed$") 
	public void contact_form_is_displayed(){
	   ContactPageObject cp = new ContactPageObject(driver); 
	   boolean contactFormDisplayed = cp.getContactForm().isDisplayed();
	  
	  if(contactFormDisplayed) { 
	      Assert.assertTrue(true);
	      log.info("contact form is been displayed");
	      System.out.println("Contact form is been displayed");
	  }
	  else {
	  System.out.println("no");
	    }
	  }
	  
  @Then("^user enter (.*) into contact email field$") 
	 public void user_enter_email_into_contact_email_field(String email) { 
	   ContactPageObject cp = new ContactPageObject(driver);
	   cp.getContactFormEmailField().sendKeys(email);
	   log.info("email is been entered into contact form");
	  }
	  
	  
  @Then("^user enter (.*) into contact name field$") 
	public void user_enter_name_into_contact_name_field(String name) { 
	  ContactPageObject cp = new ContactPageObject(driver);
	  cp.getContactFormNameField().sendKeys(name);
	  log.info("user name is been entered into contact form");
	  }
	  
  @Then("^user enter (.*) into message field$") 
	public void user_enter_message_into_message_field(String message) { 
	  ContactPageObject cp = new ContactPageObject(driver);
	  cp.getContactFormMessageField().sendKeys(message);
	  log.info("message is been entered into contact form"); 
	  }
	  
 @When("^user click on sendMessage$")
	public void user_click_on_send_Message(){ 
		ContactPageObject cp = new ContactPageObject(driver);
	    cp.getContactFormSubmitButton().click();
	    log.info("submit button is been clicked for contact form"); 
	  }
	  
 @Then("^alert should be displayed using message thanks for message$") 
   public void alert_should_be_displayed_using_message_thanks_for_message() { 
	  String alertText = driver.switchTo().alert().getText();
	  Assert.assertEquals(alertText, "Thanks for the message!!");
	  driver.switchTo().alert().accept(); 
	  }
  
 @When("^user click on Signup menu link$")
    public void user_click_on_signup_menu_link() throws InterruptedException {
	    Thread.sleep(2000);
	    
        SignUpPageObject spo = new SignUpPageObject(driver);
        spo.getSignUpMenuLink().click();
        log.info("Sign up Menu link is been clicked");
        
     }
 
 @Then("^registration form is displayed$")
    public void register_form_is_been_displayed() {
	 
	  SignUpPageObject spo = new SignUpPageObject(driver);
	  boolean formDisplayed = spo.getSignUpForm().isDisplayed();
	  if(formDisplayed) {
		  Assert.assertTrue(true);
		  log.info("Sign up form is displayed");
		  System.out.println("Sign up form is displayed");
	  }
	  else
	  {
		  System.out.println("No signup form is not displayed");
	  }
	   
    }
 
 @Then("^user enter (.*) into username field in registration form$")
    public void user_enter_username_into_registration_form(String username) {
	  SignUpPageObject spo = new SignUpPageObject(driver);
	  spo.getUsernameField().clear();
	  spo.getUsernameField().sendKeys(username);
	  log.info("username is been entered into registration form");
	  System.out.println("username is been entered into registration form");
    }
 
 
 @Then("^user enter (.*) into password field in registration form$")
   public void user_enter_password_into_registration_form(String password) {
	  SignUpPageObject spo = new SignUpPageObject(driver);
	  spo.getPasswordField().clear();
	  spo.getPasswordField().sendKeys(password);
	  log.info("password is been entered into registration form");
	  System.out.println("password is been entered into registration form");
   }
 
 @When("^user clicks on signup$")
   public void user_clicks_on_signup() {
	  SignUpPageObject spo = new SignUpPageObject(driver);
	  spo.getSignUpSubmitButton().click();
	  log.info("user clicked on signup button");
	  System.out.println("user clicked on signup button");
   }
 
 @Then("^user is needs to be validated$")
   public void user_is_needs_to_be_validated() throws InterruptedException {

	 Thread.sleep(2000);
	 
	 String alertText = driver.switchTo().alert().getText();
	 if(alertText.contains("Sign up successful.")) {
		 Assert.assertTrue(true);
		 log.info("sign up is done successfully");
		 System.out.println("Sign up is done successfully");
		 
		 driver.switchTo().alert().accept();
	 }
	 else
	 {
		 Assert.assertFalse(true);
		 System.out.println("something went wrong");
	 }
	
 }
 
 @Then("^user should not allow to register with same credentials$")
   public void user_should_not_allow_to_register_with_same_credentials() throws InterruptedException {
	 
	 Thread.sleep(2000);
	 
	 String alertText = driver.switchTo().alert().getText();
	 if(alertText.contains("This user already exist.")) {
		 Assert.assertTrue(true);
		 log.info("displayed alert"+alertText);
		 System.out.println("displayed alert"+alertText);
		 
		 driver.switchTo().alert().accept();
	 }
	 else
	 {
		 Assert.assertFalse(true);
		 System.out.println("something went wrong");
	 }
 }
 
 
  @After()
	public void on_test_complete() {
	driver.quit();
	}
	
}
