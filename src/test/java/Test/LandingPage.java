package Test;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Resources.Base;

public class LandingPage extends Base {

	public WebDriver driver;
	
	public Logger log = LogManager.getLogger(Base.class.getName());
	
	
	@BeforeTest
	public void navUrl() throws IOException {
		
		driver = initializeDriver();
		
		log.info("driver is been initialized");
		
		driver.get(prop.getProperty("url"));
		
		log.info("site url is been navigated");
		
		driver.manage().window().maximize();
		
		log.info("browser window is been maximized");
	}
	
	
	@Test
	public void T1() {
		
		String title = driver.getTitle();
		log.info("the title of page is"+title);
		System.out.println("the title of page is"+title);
	}
	
	@AfterTest
	public void terminate() {
		driver.quit();
		log.info("browser driver is been quit");
	}
	
}
