package PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignUpPageObject {

	public WebDriver driver;
	
	
	@FindBy(css="nav#narvbarx > div > ul > li:nth-Child(8)")
	public WebElement signUpMenuLink;
	public SignUpPageObject(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	public WebElement getSignUpMenuLink() {
		return signUpMenuLink;
	}
	
	
	@FindBy(css="div#signInModal > div > div")
	public WebElement signUpForm;
	public WebElement getSignUpForm() {
		return signUpForm;
	}
	
	@FindBy(css="div#signInModal > div > div > div:nth-Child(2) > form > div:nth-Child(1) > input")
	public WebElement usernameField;
	public WebElement getUsernameField() {
		return usernameField;
	}
	
	@FindBy(css="div#signInModal > div > div > div:nth-Child(2) > form > div:nth-Child(2) > input")
	public WebElement passwordField;
	public WebElement getPasswordField() {
		return passwordField;
	}

	@FindBy(css="div#signInModal > div > div > div:nth-Child(3) > button:nth-Child(2)")
	public WebElement registerSubmit;
	public WebElement getSignUpSubmitButton() {
		return registerSubmit;
	}
}
