package PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ContactPageObject {

	public WebDriver driver;
	
	
	public ContactPageObject(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(css="div#navbarExample > ul > li:nth-Child(2) > a")
	public WebElement contactMenuLink;
	public WebElement getContactMenuLink() {
		return contactMenuLink;
	}
	
	
	@FindBy(css="div#exampleModal > div > div")
	public WebElement contactForm;
	public WebElement getContactForm() {
		return contactForm;
	}
	
	
	@FindBy(css="div#exampleModal > div > div > div:nth-Child(2) > form > div:nth-Child(1) > input")
	public WebElement contactFormEmailField;
	public WebElement getContactFormEmailField() {
		return contactFormEmailField;
	}
	
	@FindBy(css="div#exampleModal > div > div > div:nth-Child(2) > form > div:nth-Child(2) > input")
	public WebElement contactFormNameField;
	public WebElement getContactFormNameField() {
		return contactFormNameField;
	}
	
	@FindBy(css="div#exampleModal > div > div > div:nth-Child(2) > form > div:nth-Child(3)  > textarea")
	public WebElement contactFormMessageField;
	public WebElement getContactFormMessageField() {
		return contactFormMessageField;
	}
	
	@FindBy(css="div#exampleModal > div > div > div:nth-Child(3)  > button:nth-Child(2)")
	public WebElement contactFormSubmitButton;
	public WebElement getContactFormSubmitButton() {
		return contactFormSubmitButton;
	}
	
}
