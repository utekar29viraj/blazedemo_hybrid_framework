package PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AboutPageObject {

	public WebDriver driver;
	
	public AboutPageObject(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(css="div#navbarExample > ul > li:nth-Child(3) > a")
	public WebElement aboutMenuLink;
	public WebElement getAboutMenuLink() {
		return aboutMenuLink;
	}
	
	@FindBy(css="div#videoModal > div > div")
	public WebElement aboutWindowPopup;
	public WebElement getAboutWindowPop() {
		return aboutWindowPopup;
	}
	
	@FindBy(css = "div#videoModal > div > div  > div:nth-Child(2) > form > div > div")
	public WebElement playVideo;
	public WebElement getVideoPlayOption() {
		return playVideo;
	}

	@FindBy(css="div#videoModal > div > div  > div:nth-Child(3) > button")
	public WebElement closeButton;
	public WebElement getCloseButton() {
		return closeButton;
	}
}
