package TestRunner;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
//import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/Features", glue="StepDefinitions", monochrome=true,
plugin = {"pretty","html:target/Report.html"})
public class TestRunner extends AbstractTestNGCucumberTests{

}
