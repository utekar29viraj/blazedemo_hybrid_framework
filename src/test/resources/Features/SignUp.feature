Feature: Automation webapplication

Scenario Outline: Registration page validation

Given user navigates to url
When  user click on Signup menu link
Then  registration form is displayed
Then  user enter <username> into username field in registration form
Then  user enter <password> into password field in registration form
When  user clicks on signup
Then  user is needs to be validated
When  user click on Signup menu link
Then  registration form is displayed
Then  user enter <username> into username field in registration form
Then  user enter <password> into password field in registration form
When  user clicks on signup
Then  user should not allow to register with same credentials


Examples:

|username|password|
|mohan4|mohan@123|