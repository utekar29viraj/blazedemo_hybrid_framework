Feature: Automation webapplication

Scenario Outline: verify the contact form is working properly

Given user navigates to url
When  user click on contact menu option
Then  contact form is displayed
Then  user enter <contactemail> into contact email field
Then  user enter <contactname> into contact name field
Then  user enter <message> into message field
When  user click on sendMessage
Then  alert should be displayed using message thanks for message

Examples:
|contactemail|contactname|message|
|test1@gmail.com|test1|test1|
|test2@gmail.com|test2|test2|